#!/usr/bin/env python
"""
Usage: solution.py [options]

Options:
  -h --help             Show this help message and exit
  -d --debug            Enable debugging prints.
"""

from collections import defaultdict
import logging
import pprint
import string

import docopt


def parse_schematic(input_data):
    input_data = input_data.strip()
    schematic = []
    for line in input_data.split("\n"):
        line = line.strip()
        schematic.append(line)
    return schematic


def parse_numbers(schematic):
    logging.debug("****** PARSING NUMBERS **********")
    numbers = {}
    for row_index, row in enumerate(schematic):
        in_number = False
        start_index = -1
        end_index = -1
        row_length = len(row)
        logging.debug(f"{row=}")
        for index, character in enumerate(row):
            if character not in string.digits and not in_number:
                continue
            elif character in string.digits and not in_number:
                logging.debug(f"Entering number with {character=} ({index=})")
                in_number = True
                start_index = index
            elif character in string.digits and in_number:
                logging.debug(f"Continuing number with {character=} ({index=})")
                pass
            elif character not in string.digits and in_number:
                logging.debug(
                    f"End of row, ending number of number, now at {character=} ({index=})"
                )
                end_index = index
                this_number = int(row[start_index:end_index])
                numbers[(row_index, start_index, end_index)] = this_number
                in_number = False
                start_index = -1
                end_index = -1
            if index + 1 == row_length and in_number:
                logging.debug(
                    f"Row ended completing number, now at {character=} ({index=})"
                )
                # catch the end of the row
                end_index = index + 1
                this_number = int(row[start_index:end_index])
                numbers[(row_index, start_index, end_index)] = this_number
                in_number = False
                start_index = -1
                end_index = -1
    logging.debug(f"numbers =\n{pprint.pformat(numbers)}")
    logging.debug("****** DONE PARSING NUMBERS **********")
    return numbers


def characters_to_check(schematic, row, just_before, just_after):
    characters = []
    if row < 0 or row > len(schematic):
        # first or last row
        return characters
    for this_index in range(just_before, just_after + 1):
        try:
            this_character = schematic[row][this_index]
            characters.append(this_character)
        except IndexError:
            pass
    return characters


def valid_number(location, schematic):
    row_index, start_index, end_index = location
    # Check left
    row_length = len(schematic[row_index])
    try:
        if start_index - 1 < 0:
            # out of the grid
            pass
        elif schematic[row_index][start_index - 1] != ".":
            logging.debug("True left")
            return True
        else:
            logging.debug(f"l: {schematic[row_index][start_index - 1]}")
    except IndexError:
        pass
    # Check right
    try:
        if end_index > row_length:
            # out of the grid
            pass
        elif schematic[row_index][end_index] != ".":
            logging.debug("True right")
            return True
        else:
            logging.debug(f"r: {schematic[row_index][end_index]}")
    except IndexError:
        pass
    # Check above
    for character in characters_to_check(
        schematic, row_index - 1, start_index - 1, end_index
    ):
        # Might get a number diagonal/above/below
        logging.debug(f"a: {character}")
        if character not in "0123456789.":
            logging.debug("True above")
            return True
    # Check below
    for character in characters_to_check(
        schematic, row_index + 1, start_index - 1, end_index
    ):
        # Might get a number diagonal/above/below
        logging.debug(f"b: {character}")
        if character not in "0123456789.":
            logging.debug("True below")
            return True
    logging.debug("Number is not valid.")
    return False


def sum_valid_numbers(numbers, schematic):
    sum_valid = 0
    logging.debug("STARTING TO CHECK")
    for location in numbers:
        logging.debug(f"******** CHECKING {numbers[location]}")
        valid = valid_number(location, schematic)
        if valid:
            logging.debug(f"{numbers[location]} is valid")
            sum_valid += numbers[location]
    return sum_valid


def part_1(input_data):
    schematic = parse_schematic(input_data)
    numbers = parse_numbers(schematic)
    answer = sum_valid_numbers(numbers, schematic)
    return answer


def part_2(input_data):
    schematic = parse_schematic(input_data)
    numbers = parse_numbers(schematic)
    answer = None
    return answer


def main():
    with open("input.txt") as input_file:
        input_data = input_file.read()
    print(part_1(input_data))
    print(part_2(input_data))


if __name__ == "__main__":
    options = docopt.docopt(__doc__)
    if options["--debug"]:
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")
    main()
