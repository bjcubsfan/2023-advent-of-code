import logging
import pytest

from solution import part_1, part_2

input_data = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""

input_data_two = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""


# @pytest.mark.parametrize(
#    "encrypted_room, is_real_room",
#    [
#        ("aaaaa-bbb-z-y-x-123[abxyz]", True),
#        ("a-b-c-d-e-f-g-h-987[abcde]", True),
#        ("not-a-real-room-404[oarel]", True),
#        ("totally-real-room-200[decoy]", False),
#    ],
# )
def test_part_1():
    calc_part_1 = part_1(input_data)
    assert calc_part_1 == 142


def test_part_2():
    calc_part_2 = part_2(input_data_two)
    assert calc_part_2 == 281
