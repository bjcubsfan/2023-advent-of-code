#!/usr/bin/env python
"""
Usage: solution.py [options]

Options:
  -h --help             Show this help message and exit
  -d --debug            Enable debugging prints.
"""

from collections import defaultdict
import logging
import pprint

import docopt


def parse_input_line(line):
    game_and_id, rest = line.split(":")
    junk, game_id = game_and_id.split()
    game_id = int(game_id)
    game = list()
    for single_draw_string in rest.split(";"):
        single_draw_string = single_draw_string.strip()
        raw_draws = single_draw_string.split(",")
        draw = dict()
        for color_draw in raw_draws:
            number, color = color_draw.split()
            draw[color] = int(number)
        game.append(draw)
    logging.debug(f"Game {game_id}:\n{pprint.pformat(game)}")
    return game_id, game


def game_valid(game, actuals):
    for draw in game:
        # logging.debug(f"{draw=}")
        for draw_color, draw_number in draw.items():
            if draw_number > actuals[draw_color]:
                return False
    return True


def part_1(input_data):
    input_data = input_data.strip()
    answer = 0
    actuals = {"red": 12, "green": 13, "blue": 14}
    for line in input_data.split("\n"):
        line = line.strip()
        game_id, game = parse_input_line(line)
        if game_valid(game, actuals):
            logging.debug(f"Game {game_id} is valid.")
            answer += game_id
    return answer


def game_power(game):
    maxes = dict()
    for draw in game:
        for draw_color, draw_number in draw.items():
            if draw_number > maxes.get(draw_color, 0):
                maxes[draw_color] = draw_number
    power = None
    for max_color, max_number in maxes.items():
        if not power:
            power = max_number
            continue
        else:
            power *= max_number
    logging.debug(f"Game:\n{pprint.pformat(game)}" f"\n has power of {power}")
    return power


def part_2(input_data):
    input_data = input_data.strip()
    answer = 0
    for line in input_data.split("\n"):
        line = line.strip()
        game_id, game = parse_input_line(line)
        answer += game_power(game)
    return answer


def main():
    with open("input.txt") as input_file:
        input_data = input_file.read()
    print(part_1(input_data))
    print(part_2(input_data))


if __name__ == "__main__":
    options = docopt.docopt(__doc__)
    if options["--debug"]:
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")
    main()
