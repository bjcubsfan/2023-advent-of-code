# 2023 advent of code

My attempts at completing the [2023 advent of code](https://adventofcode.com/2023).

Run tests in each directory with:

```
pytest -vs --log-cli-level debug
```
