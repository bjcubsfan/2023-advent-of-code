#!/usr/bin/env python
"""
Usage: solution.py [options]

Options:
  -h --help             Show this help message and exit
  -d --debug            Enable debugging prints.
"""

from collections import defaultdict
import logging
import pprint

import docopt


def make_ints(numbers):
    numbers = numbers.strip()
    ints = set()
    for number in numbers.split():
        ints.add(int(number.strip()))
    return ints


def points_on_card(line):
    card_title, card_numbers = line.split(":")
    card_numbers = card_numbers.strip()
    winning_numbers, have_numbers = card_numbers.split("|")
    winning_ints = make_ints(winning_numbers)
    have_ints = make_ints(have_numbers)
    logging.debug(f"{winning_ints=}")
    logging.debug(f"{have_ints=}")
    num_winners = len(winning_ints & have_ints)
    if num_winners:
        return 2 ** (num_winners - 1)
    else:
        return 0


def part_1(input_data):
    input_data = input_data.strip()
    answer = 0
    for line in input_data.split("\n"):
        line = line.strip()
        answer += points_on_card(line)
    return answer


def part_2(input_data):
    input_data = input_data.strip()
    answer = None
    for line in input_data.split("\n"):
        line = line.strip()
    return answer


def main():
    with open("input.txt") as input_file:
        input_data = input_file.read()
    print(part_1(input_data))
    print(part_2(input_data))


if __name__ == "__main__":
    options = docopt.docopt(__doc__)
    if options["--debug"]:
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")
    main()
