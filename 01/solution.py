#!/usr/bin/env python
"""
Usage: solution.py [options]

Options:
  -h --help             Show this help message and exit
  -d --debug            Enable debugging prints.
"""

from collections import defaultdict
import logging
import string
from pprint import pprint

import docopt


def part_1(input_data):
    input_data = input_data.strip()
    answer = 0
    for line in input_data.split("\n"):
        line = line.strip()
        for first_digit in line:
            if first_digit in string.digits:
                break
        for second_digit in reversed(line):
            if second_digit in string.digits:
                break
        answer += int(f"{first_digit}{second_digit}")
    return answer


def is_a_digit(index, line):
    word_digits = {
        "one": "1",
        "two": "2",
        "three": "3",
        "four": "4",
        "five": "5",
        "six": "6",
        "seven": "7",
        "eight": "8",
        "nine": "9",
    }
    if line[index] in string.digits:
        return line[index]
    part_to_check = line[index:]
    for name, number in word_digits.items():
        if part_to_check.startswith(name):
            logging.debug(f"HIT on {part_to_check}, returning {number}")
            return number
    return False


def part_2(input_data):
    input_data = input_data.strip()
    answer = 0
    for line in input_data.split("\n"):
        logging.debug(f"STARTING LINE: {line}")
        line = line.strip()
        what_to_check = list(enumerate(line))
        for index, first_digit in what_to_check:
            if first_digit in string.digits:
                logging.debug(f"numeral {first_digit=}")
                break
            else:
                first_digit = is_a_digit(index, line)
                if first_digit:
                    logging.debug(f"word {first_digit=}")
                    break
        for index, second_digit in reversed(what_to_check):
            if second_digit in string.digits:
                logging.debug(f"numeral {second_digit=}")
                break
            else:
                second_digit = is_a_digit(index, line)
                if second_digit:
                    logging.debug(f"word {second_digit=}")
                    break
        answer += int(f"{first_digit}{second_digit}")
    return answer


def main():
    with open("input.txt") as input_file:
        input_data = input_file.read()
    print(part_1(input_data))
    print(part_2(input_data))


if __name__ == "__main__":
    options = docopt.docopt(__doc__)
    if options["--debug"]:
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")
    main()
